import React from "react";
import { NavLink } from "react-router-dom";
import UserNav from "./UserNav";

export default function Header({ children }) {
  return (
    <div className="flex px-10 py-5 shadow justify-between items-center">
      <NavLink to={"/"}>
        <span className="italic text-red-400 font-medium text-4xl">
          CyberFlix {children}
        </span>
      </NavLink>
      <UserNav />
    </div>
  );
}
