import { SET_USER_INFOR } from "./../constant/userConstant";
import { userService } from "./../../Services/userService";
import { userLocaService } from "../../Services/localStorageService";
import { message } from "antd";

export const setLoginAction = (value) => {
  //api here
  return {
    type: SET_USER_INFOR,
    payload: value,
  };
};

export const setLoginActionService = (userForm, onSuccess) => {
  return (dispatch) => {
    userService
      .postDangNhap(userForm)
      .then((res) => {
        message.success("Đăng nhập thành công");
        // Lưu vào localStore
        userLocaService.set(res.data.content);
        console.log(res);
        // Đẩy lên redux store
        dispatch({
          type: SET_USER_INFOR,
          payload: res.data.content,
        });
        onSuccess();
      })
      .catch((err) => {
        message.error("Đã có lỗi xảy ra");
        console.log(err);
      });
  };
};
