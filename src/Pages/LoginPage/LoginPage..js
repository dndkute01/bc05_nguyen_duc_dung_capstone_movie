import { Button, Checkbox, Form, Input, message } from "antd";
import React from "react";
import { useDispatch } from "react-redux";
import { useNavigate } from "react-router-dom";
import { userService } from "./../../Services/userService";
import { SET_USER_INFOR } from "./../../redux/constant/userConstant";
import { userLocaService } from "../../Services/localStorageService";

import Lottie from "lottie-react";
import Chrismas from "../../assets/90315-christmas-tree.json";
import {
  setLoginAction,
  setLoginActionService,
} from "./../../redux/actions/userAction";
import { setUserInfor } from "../../redux-toolkit/slice/userSlice";

export default function LoginPage() {
  let navigate = useNavigate();
  let dispatch = useDispatch();
  const onFinish = (values) => {
    console.log("Success:", values);
    userService
      .postDangNhap(values)
      .then((res) => {
        dispatch(setUserInfor(res.data.content));

        console.log(res);
        message.success("Đăng nhập thành công");
        // Lưu vào localStore
        userLocaService.set(res.data.content);
        // Chuyển hướng về trang chủ nhưng cần load trang :  window.location.href="/"

        setTimeout(() => {
          // Dùng navigate để không cần load lại trang
          navigate("/");
        }, 1000);
      })
      .catch((err) => {
        console.log(err);
        message.error("Đã có lỗi xảy ra");
      });
  };
  const onFinishReduxThunk = (value) => {
    let onNavigate = () => {
      setTimeout(() => {
        navigate("/");
      }, 1000);
    };
    dispatch(setLoginActionService(value, onNavigate));
  };
  const onFinishFailed = (errorInfo) => {
    console.log("Failed:", errorInfo);
  };
  return (
    <div className="w-screen h-screen flex justify-center items-center">
      <div className="container p-5 flex" style={{ width: "80%" }}>
        <div className="h-full w-1/2">
          <Lottie animationData={Chrismas} loop={true} />
        </div>
        <div className="h-full w-1/2 pt-20">
          <Form
            name="basic"
            labelCol={{
              span: 8,
            }}
            wrapperCol={{
              span: 24,
            }}
            initialValues={{
              remember: true,
            }}
            onFinish={onFinish}
            onFinishFailed={onFinishFailed}
            autoComplete="off"
            layout="vertical"
          >
            <Form.Item
              label="Username"
              name="taiKhoan"
              rules={[
                {
                  required: true,
                  message: "Please input your username!",
                },
              ]}
            >
              <Input />
            </Form.Item>

            <Form.Item
              label="Password"
              name="matKhau"
              rules={[
                {
                  required: true,
                  message: "Please input your password!",
                },
              ]}
            >
              <Input.Password />
            </Form.Item>

            <Form.Item
              wrapperCol={{
                span: 24,
              }}
              className="text-center"
            >
              <Button
                className="bg-green-500 text-white hover:bg-white"
                htmlType="submit"
              >
                Submit
              </Button>
            </Form.Item>
          </Form>
        </div>
      </div>
    </div>
  );
}
