import React, { useEffect, useState } from "react";
import { adminService } from "./../../Services/adminService";

//
import { Space, Table, Tag, message } from "antd";
import { columnsUser } from "./utils";

const columns = [
  {
    title: "Name",
    dataIndex: "name",
    key: "name",
    render: (text) => <a>{text}</a>,
  },
  {
    title: "Age",
    dataIndex: "age",
    key: "age",
  },
  {
    title: "Address",
    dataIndex: "address",
    key: "address",
  },
  {
    title: "Tags",
    key: "tags",
    dataIndex: "tags",
    render: (_, { tags }) => (
      <>
        {tags.map((tag) => {
          let color = tag.length > 5 ? "geekblue" : "green";
          if (tag === "loser") {
            color = "volcano";
          }
          return (
            <Tag color={color} key={tag}>
              {tag.toUpperCase()}
            </Tag>
          );
        })}
      </>
    ),
  },
  {
    title: "Action",
    key: "action",
    render: (_, record) => (
      <Space size="middle">
        <a>Invite {record.name}</a>
        <a>Delete</a>
      </Space>
    ),
  },
];
const data = [
  {
    key: "1",
    name: "John Brown",
    age: 32,
    address: "New York No. 1 Lake Park",
    tags: ["nice", "developer"],
  },
  {
    key: "2",
    name: "Jim Green",
    age: 42,
    address: "London No. 1 Lake Park",
    tags: ["loser"],
  },
  {
    key: "3",
    name: "Joe Black",
    age: 32,
    address: "Sidney No. 1 Lake Park",
    tags: ["cool", "teacher"],
  },
];

//

export default function UserAdminPage() {
  const [userArr, setUserArr] = useState([]);

  useEffect(() => {
    let handleDeleteUser = (idUser) => {
      adminService
        .xoaNguoiDung(idUser)
        .then((res) => {
          console.log(res);
          message.success("Xóa người dùng thành công");
        })
        .catch((err) => {
          message.error(err.response.data.content);
          console.log(err);
        });
    };

    let fetchUserList = () => {
      adminService
        .layDanhSachNguoiDung()
        .then((res) => {
          // console.log(res);
          var userList = res.data.content.map((item) => {
            return {
              ...item,
              action: (
                <>
                  <button
                    onClick={() => {
                      handleDeleteUser(item.taiKhoan);
                    }}
                    className="px-2 py-1 rounded bg-red-500 text-white"
                  >
                    Xóa
                  </button>
                  <button className="px-2 py-1 rounded bg-blue-500 text-white">
                    Sửa
                  </button>
                </>
              ),
            };
          });
          setUserArr(userList);
        })
        .catch((err) => {
          console.log(err);
        });
    };
    fetchUserList();
  }, []);

  return (
    <div className="container mx-auto">
      <Table columns={columnsUser} dataSource={userArr} />
    </div>
  );
}
