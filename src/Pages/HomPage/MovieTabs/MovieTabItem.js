import moment from "moment/moment";
import React from "react";

export default function MovieTabItem({ movie }) {
  console.log("movie: ", movie);
  return (
    <div className="flex p-3">
      <img
        className="w-24 h-40 object-cover mr-5 rounded"
        src={movie.hinhAnh}
        alt=""
      />
      <div>
        <h5 className="font-medium mb-5 text-green-600 text-xl">
          {movie.tenPhim}
        </h5>
        <div className="grid grid-cols-3 gap-4">
          {movie.lstLichChieuTheoPhim.slice(0, 9).map((item) => {
            return (
              <span className="bg-orange-600 text-white rounded p-2">
                {moment(item.ngayChieuGioChieu).format("DD/MM/YYYY - hh:mm A")}
              </span>
            );
          })}
        </div>
      </div>
    </div>
  );
}
