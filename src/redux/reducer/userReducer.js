import { userLocaService } from "../../Services/localStorageService";
import { SET_USER_INFOR } from "../constant/userConstant";

const initialState = {
  userInfor: userLocaService.get(),
};

export const userReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case SET_USER_INFOR:
      return { ...state, userInfor: payload };

    default:
      return state;
  }
};

// rxreducer
