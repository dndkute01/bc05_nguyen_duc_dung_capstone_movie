import React from "react";
import { useSelector } from "react-redux";
import { NavLink } from "react-router-dom";
import { userLocaService } from "../../Services/localStorageService";
import { userReducer } from "./../../redux/reducer/userReducer";
import { userSlice } from "./../../redux-toolkit/slice/userSlice";

export default function UserNav() {
  let user = useSelector((state) => {
    return state.userSlice.userInfor;
  });

  let handleLogOut = () => {
    userLocaService.remove();
    window.location.href = "/login";
  };

  const renderContent = () => {
    if (user) {
      return (
        <>
          <span className=" text-2xl text-emerald-700 align-middle font-medium">
            {user.hoTen}
          </span>
          <button
            onClick={handleLogOut}
            class="bg-transparent hover:bg-red-500 text-red-700 font-semibold hover:text-white py-2 px-4 border border-red-500 hover:border-transparent rounded"
          >
            <NavLink>Đăng xuất</NavLink>
          </button>
        </>
      );
    } else {
      return (
        <>
          <button class="bg-transparent hover:bg-green-500 text-green-700 font-semibold hover:text-white py-2 px-4 border border-green-500 hover:border-transparent rounded ">
            <NavLink to={"/login"}>Đăng nhập</NavLink>
          </button>
          <button class="bg-transparent hover:bg-yellow-500 text-yellow-700 font-semibold hover:text-white py-2 px-4 border border-yellow-500 hover:border-transparent rounded">
            Đăng kí
          </button>
        </>
      );
    }
  };

  return <div className="space-x-5">{renderContent()}</div>;
}
