import { Tag } from "antd";

export const columnsUser = [
  { title: "Tài khoản", dataIndex: "taiKhoan", key: "taiKhoan" },
  { title: "Họ tên", dataIndex: "hoTen", key: "hoTen" },
  { title: "Email", dataIndex: "email", key: "email" },
  {
    title: "Loại khách",
    dataIndex: "maLoaiNguoiDung",
    key: "maLoaiNguoiDung",
    render: (po) => {
      if (po == "QuanTri") {
        return <Tag color="red">Quản Trị</Tag>;
      } else {
        return <Tag color="blue">Khách hàng</Tag>;
      }
    },
  },
  { title: "Thao tác", dataIndex: "action", key: "action" },
];
