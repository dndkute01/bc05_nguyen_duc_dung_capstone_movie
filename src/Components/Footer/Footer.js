import React from "react";
import { Desktop, Mobile, Tablet } from "../../HOC/Responsive";
import FooterDestop from "./FooterDestop";
import FooterTablet from "./FooterTablet";
import FooterMobile from "./FooterMobile";

export default function Footer() {
  return (
    <div>
      <Desktop>
        <FooterDestop />
      </Desktop>

      <Tablet>
        <FooterTablet />
      </Tablet>

      <Mobile>
        <FooterMobile />
      </Mobile>
    </div>
  );
}
