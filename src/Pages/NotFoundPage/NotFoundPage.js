import "./notFound.css";

import React from "react";

export default function NotFoundPage() {
  return (
    <div className="flex justify-center items-center h-screen w-screen">
      <h4 className="text-7xl text-red-700 font-bold animate-pulse">404</h4>
    </div>
  );
}
