import React, { useEffect, useState } from "react";
import { movieService } from "./../../Services/movieService";
import MovieList from "./MovieList/MovieList";
import MovieTabs from "./MovieTabs/MovieTabs";

export default function HomePage() {
  const [movieArr, setMovieArr] = useState([]);
  useEffect(() => {
    movieService
      .getDanhSachPhim()
      .then((res) => {
        setMovieArr(res.data.content);
      })
      .catch((err) => {
        console.log(err);
      });
  }, []);

  return (
    <div className="container mx-auto " style={{ width: "95%" }}>
      <MovieList movieArr={movieArr} />
      <MovieTabs />
      <br />
      <br />
      <br />
      <br />
      <br />
      <br />
      <br />
      <br />
      <br />
      <br />
      <br />
      <br />
      <br />
      <br />
      <br />
      <br />
      <br />
      <br />
      <br />
      <br />
    </div>
  );
}
