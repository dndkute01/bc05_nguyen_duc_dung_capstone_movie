import React from "react";
import { Card } from "antd";
import { NavLink } from "react-router-dom";
const { Meta } = Card;

export default function MovieList({ movieArr }) {
  let renderDanhSachPhim = () => {
    return movieArr.slice(0, 15).map((item, index) => {
      let { hinhAnh, tenPhim, moTa, maPhim } = item;
      return (
        <Card
          key={index}
          hoverable
          style={{
            width: "100%",
          }}
          cover={
            <img className="h-80 object-cover" alt="example" src={hinhAnh} />
          }
        >
          <Meta
            title={tenPhim}
            description={moTa.length < 60 ? moTa : moTa.slice(0, 60) + "..."}
          />
          <NavLink to={`/detail/${maPhim}`}>
            <button className="bg-transparent hover:bg-green-500 text-green-700 font-semibold hover:text-white py-2 px-4 border border-green-500 hover:border-transparent rounded">
              DETAIL
            </button>
          </NavLink>
        </Card>
      );
    });
  };
  return <div className="grid grid-cols-5 gap-5 ">{renderDanhSachPhim()}</div>;
}
