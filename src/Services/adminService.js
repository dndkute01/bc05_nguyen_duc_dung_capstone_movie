import { https } from "./configURL";

export const adminService = {
  layDanhSachNguoiDung: () => {
    return https.get("/api/QuanLyNguoiDung/LayDanhSachNguoiDung");
  },

  xoaNguoiDung: (idUser) => {
    return https.delete(`/api/QuanLyNguoiDung/XoaNguoiDung?TaiKhoan=${idUser}`);
  },
};

//  axios instant
