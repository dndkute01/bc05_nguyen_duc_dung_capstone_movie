import { createSlice } from "@reduxjs/toolkit";
import { userLocaService } from "../../Services/localStorageService";

const initialState = {
  userInfor: userLocaService.get(),
};

const userSlice = createSlice({
  name: "userSlice",
  initialState,
  reducers: {
    setUserInfor: (state, action) => {
      state.userInfor = action.payload;
    },
  },
});

export const { setUserInfor } = userSlice.actions;

export default userSlice.reducer;
